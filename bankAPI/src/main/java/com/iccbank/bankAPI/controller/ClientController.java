package com.iccbank.bankAPI.controller;

import com.iccbank.bankAPI.model.Client;
import com.iccbank.bankAPI.model.ClientProductSubscription;
import com.iccbank.bankAPI.model.Product;
import com.iccbank.bankAPI.services.BankService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationException;
import java.util.List;

@RestController
public class ClientController {
    @Autowired
    private BankService bankService;

    public ClientController(BankService bankService) {
        this.bankService = bankService;
    }

    @ApiOperation( value = "Ajouter un nouveau client" )
    @RequestMapping( method = RequestMethod.POST, value = "Clients/AddClient" )
    @ResponseBody
    public ResponseEntity<Client> AddNewClient(@RequestBody String name) throws ValidationException {

            Client addedClient = bankService.AddClientByName(name, 1);
            return new ResponseEntity<>(addedClient, HttpStatus.OK);
    }

    @ApiOperation(value = "Obtenir tous les clients de la banque")
    @RequestMapping(method= RequestMethod.GET, value="Clients/getAllClients")
    public List<Client> getAllClients() {
        return bankService.GetAllClients();
    }

    @ApiOperation(value = "Obtenir les souscriptions des clients")
    @RequestMapping(method= RequestMethod.GET, value="Clients/getSuscriptions")
    public List<ClientProductSubscription> getClientSuscriptions(String clientName) {
        return bankService.getClientProductSubscriptions(clientName);
    }

    @ApiOperation(value = "Obtenir les produits disponibles pour un cleint")
    @RequestMapping(method= RequestMethod.GET, value="Clients/getClientAvailableProducts")
    public List<Product> getClientAvailableProducts(String clientName) {
        return bankService.getClientProductsAvailable(clientName);
    }
}
