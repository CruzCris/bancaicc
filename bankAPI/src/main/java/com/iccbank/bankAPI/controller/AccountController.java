package com.iccbank.bankAPI.controller;

import com.iccbank.bankAPI.model.Client;
import com.iccbank.bankAPI.model.ClientProductSubscription;
import com.iccbank.bankAPI.model.Product;
import com.iccbank.bankAPI.services.BankService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationException;
import java.util.List;

@RestController
public class AccountController {
    @Autowired
    private BankService bankService;

    public AccountController(BankService bankService) {
        this.bankService = bankService;
    }

    @ApiOperation(value = "Obtenir les produits disponibles pour un client")
    @RequestMapping(method= RequestMethod.GET, value="Account/getClientAvailableProducts")
    @ResponseBody
    public ResponseEntity<List<Product>>getClientAvailableProducts(String clientName) {

            List<Product> products = bankService.getClientProductsAvailable(clientName);
            return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @ApiOperation(value = "Obtenir les souscriptions des clients")
    @RequestMapping(method= RequestMethod.GET, value="Account/getSuscriptions")
    public List<ClientProductSubscription> getClientSuscriptions(String clientName) {
        return bankService.getClientProductSubscriptions(clientName);
    }

}
