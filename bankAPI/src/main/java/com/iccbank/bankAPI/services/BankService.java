package com.iccbank.bankAPI.services;

import com.iccbank.bankAPI.model.Client;
import com.iccbank.bankAPI.model.ClientProductSubscription;
import com.iccbank.bankAPI.model.ClientStatus;
import com.iccbank.bankAPI.model.Product;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.xml.bind.ValidationException;
import java.util.ArrayList;
import java.util.List;

@Service
public class BankService implements IBankService {
    private List<Client> clients;
    private List<Product> products;

    public BankService() {
        clients = new ArrayList<>();
        products = new ArrayList<>();

        InitializeProducts();
    }

    private void InitializeProducts() {

        int id = 1;

        products.add(new Product(id++, "Visa Debit Card", ClientStatus.BASIC, 1));
        products.add(new Product(id++, "Visa Credit Card", ClientStatus.BRONZE, 0));
        products.add(new Product(id++, "Mastercard Basic Credit Card", ClientStatus.BRONZE, 1));
        products.add(new Product(id++, "Mastercard SILVER Credit Card", ClientStatus.SILVER, 0));
        products.add(new Product(id++, "Mastercard GOLD Credit Card", ClientStatus.GOLD, 1));
        products.add(new Product(id++, "Mastercard Rewards Credit CArd", ClientStatus.DIAMOND, 1));
        products.add(new Product(id++, "Basic Savings Account", ClientStatus.BASIC, 0));
        products.add(new Product(id++, "Interest Savings Account", ClientStatus.BRONZE, 1));
        products.add(new Product(id++, "Long Term Savings Account", ClientStatus.DIAMOND, 0));
    }

    public Client AddClientByName(String name, int status) throws ValidationException {
        if (!name.trim().isEmpty()) {
            if (ClientStatus.getStatusByValue(status) != null) {
                Client newClient = new Client();
                newClient.setName(name);
                newClient.setStatus(ClientStatus.getStatusByValue(status));
                clients.add(newClient);

                return newClient;
            } else {
                throw new ValidationException("La valeur du statut est invalide");
            }
        } else {
            throw new ValidationException("Le nom du client n'est pas présent ou il est invalide");
        }

    }

    @Override
    public List<Client> GetAllClients() {
        return clients;
    }

    @Override
    public List<ClientProductSubscription> getClientProductSubscriptions(String clientName) {
        Client client = findClient(clientName);
        if (client !=  null)
                return client.getProductSubscriptions();
        else {
            //TODO client not found message
            return null;
        }
}

    @Override
    public List<Product> getClientProductsAvailable(String clientName) {
        Client client = findClient(clientName);
        if (client !=null){
            int statusValue = client.getStatus().getStatusValue();
            List<Product> availableProducts = new ArrayList<>();
            for (Product p: products) {
                if (p.getMinStatusRequired().getStatusValue()  <= statusValue)
                    availableProducts.add(p);
            }
            return availableProducts;
        }
        return null;
    }

      private Client findClient(String clientName){
        for (Client client : clients)
            if (client.getName().toUpperCase().equals(clientName.trim().toUpperCase())) {
                return client;
            }
        return null;
    }
}


