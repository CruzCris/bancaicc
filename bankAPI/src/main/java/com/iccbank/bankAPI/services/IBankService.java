package com.iccbank.bankAPI.services;

import com.iccbank.bankAPI.model.Client;
import com.iccbank.bankAPI.model.ClientProductSubscription;
import com.iccbank.bankAPI.model.Product;

import javax.xml.bind.ValidationException;
import java.util.List;

public interface IBankService {
    Client AddClientByName(String name, int status) throws ValidationException;

    List<Client> GetAllClients();

    List<ClientProductSubscription> getClientProductSubscriptions(String clientName);

    List<Product> getClientProductsAvailable(String clientName);
}
