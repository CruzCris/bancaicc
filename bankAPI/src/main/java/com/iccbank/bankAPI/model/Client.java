package com.iccbank.bankAPI.model;

import java.util.ArrayList;
import java.util.List;

public class Client {
    private String Name;

    private ClientStatus Status;

    private List<ClientProductSubscription> productSubscriptions;

    public Client() {
        productSubscriptions = new ArrayList<>();
    }


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        if (!name.isEmpty())
            Name = name;
    }

    public ClientStatus getStatus() {
        return Status;
    }

    public void setStatus(ClientStatus status) {
        Status = status;
    }
    public List<ClientProductSubscription> getProductSubscriptions() {
        return productSubscriptions;
    }

}
