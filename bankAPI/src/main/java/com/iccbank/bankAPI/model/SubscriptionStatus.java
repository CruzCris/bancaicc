package com.iccbank.bankAPI.model;

public enum SubscriptionStatus {
    PENDING_APPROUVAL,
    APPROUVED,
    PENDING_UNSUBSCRIBE,
    CANCELED,
    SUBSCRIPTION_REJECTED,
    UNSUBSCRIPTION_REJECTED
}
