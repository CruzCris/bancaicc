package com.iccbank.bankAPI.model;

public enum ClientStatus {
    BASIC(1),
    BRONZE(2),
    SILVER(3),
    GOLD(4),
    DIAMOND(5);

    private final int levelValue;

    ClientStatus(int levelValue) {
        this.levelValue = levelValue;
    }

    public static ClientStatus getStatusByValue(int levelValue) {
        ClientStatus[] status = ClientStatus.values();

        for (ClientStatus value : status) {
            if (value.levelValue == levelValue) {
                return value;
            }
        }
        return null;
    }

    public int getStatusValue() {
        return this.levelValue;
    }
}

