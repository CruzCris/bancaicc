package com.iccbank.bankAPI.model;

public class Product {

    private int ID ;

    private String nom;

    private ClientStatus minStatusRequired;

    private int ApprobationType;

    public Product(int ID, String nom, ClientStatus minStatusRequired, int approbationType) {
        this.ID = ID;
        this.nom = nom;
        this.minStatusRequired = minStatusRequired;
        ApprobationType = approbationType;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setMinStatusRequired(ClientStatus minStatusRequired) {
        this.minStatusRequired = minStatusRequired;
    }

    public void setApprobationType(int approbationType) {
        ApprobationType = approbationType;
    }

    public String getNom() {
        return nom;
    }

    public ClientStatus getMinStatusRequired() {
        return minStatusRequired;
    }

    public int getApprobationType() {
        return ApprobationType;
    }



}
