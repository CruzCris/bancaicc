package com.iccbank.bankAPI.integration;


import com.iccbank.bankAPI.BankApiApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest(classes = BankApiApplication.class)
@ContextConfiguration
public class CucumberSpringIntegrationTest {
    @Test
    void contextLoads() {
    }
}
