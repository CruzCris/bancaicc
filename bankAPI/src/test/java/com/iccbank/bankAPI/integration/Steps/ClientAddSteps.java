package com.iccbank.bankAPI.integration.Steps;

import com.iccbank.bankAPI.integration.CucumberSpringIntegrationTest;
import com.iccbank.bankAPI.model.Client;
import com.iccbank.bankAPI.model.ClientStatus;
import com.iccbank.bankAPI.services.BankService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.mockito.InjectMocks;

import javax.xml.bind.ValidationException;

public class ClientAddSteps extends CucumberSpringIntegrationTest {

    String client;
    Client returnedClient;

    @InjectMocks
    BankService bankService;

    @Given( "Client du Service veut ajouter un nouveau client avec le nom {string}" )
    public void clientDuServiceVeutAjouterUnNouveauClientAvecLeNom(String arg0){
        client = arg0;
    }

    @When( "Client du Service envoi le nom du nouveau client" )
    public void clientDuServiceEnvoiLeNomDuNouveauClient() throws ValidationException {
        int statusToAdd = 1;
        bankService = new BankService();
        returnedClient = bankService.AddClientByName(client, statusToAdd);
    }

     @Then( "le client est sauvegarde" )
    public void leClientEstSauvegarde() {
        Assert.assertTrue(returnedClient.getName() == client);
        Assert.assertTrue(returnedClient.getStatus() == ClientStatus.BASIC);
    }
}
