package com.iccbank.bankAPI.services;

import com.iccbank.bankAPI.model.Client;
import com.iccbank.bankAPI.model.ClientProductSubscription;
import com.iccbank.bankAPI.model.ClientStatus;
import com.iccbank.bankAPI.model.Product;
import com.iccbank.bankAPI.services.BankService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import javax.xml.bind.ValidationException;
import java.util.List;

/**
 * BankService Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Mar 15, 2020</pre>
 */
public class BankServiceTest {

    @InjectMocks
    BankService bankService;

    @Before
    public void before() throws Exception {

        bankService = new BankService();
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: AddClient(Client client)
     */
    @Test
    public void testAddClientByName() throws Exception {
        String newClientName = "Client 1";
        int statusToAdd = 1;
        Client clientReturn = bankService.AddClientByName(newClientName, statusToAdd);

        Assert.assertTrue(clientReturn.getName() == newClientName);
        Assert.assertTrue(clientReturn.getStatus() == ClientStatus.BASIC);
    }

    @Test
    public void testAddClientWithEmptyName() throws Exception {
        try {
            String newClientName = "";
            int statusToAdd = 1;
            Client clientReturn = bankService.AddClientByName(newClientName, statusToAdd);
            Assert.fail("L'ajout du client ne doit pas s'effectuer");
        } catch (ValidationException e) {
            Assert.assertEquals("Le nom du client n'est pas présent ou il est invalide", e.getMessage());
        }
    }

    @Test
    public void testAddClientWithInvalidStatus() throws Exception {
        try {
            String newClientName = "Client 1";
            int statusToAdd = 0;
            Client clientReturn = bankService.AddClientByName(newClientName, statusToAdd);
            Assert.fail("L'ajout du client ne doit pas s'effectuer");
        } catch (ValidationException e) {
            Assert.assertEquals("La valeur du statut est invalide", e.getMessage());
        }

    }


    @Test
    public void getAllClients() {
        String newClientName = "Client 1";
        int statusToAdd = 1;
        try {
            bankService.AddClientByName(newClientName, 1);

            List<Client> listResult = bankService.GetAllClients();

            Assert.assertTrue(listResult.size() == 1);
            Assert.assertTrue(listResult.get(0).getName() == newClientName);
            Assert.assertTrue(listResult.get(0).getStatus() == ClientStatus.BASIC);

        } catch (ValidationException e) {
            Assert.fail("Erreur lors de l'éxécution de la méthode");
        }

    }


    @Test
    public void getClientProductSubscriptions() {
        String newClientName = "Client 1";
        int statusToAdd = 1;
        try {
            bankService.AddClientByName(newClientName, 1);

            List<ClientProductSubscription> list = bankService.getClientProductSubscriptions(newClientName);

            Assert.assertTrue(list != null);
            Assert.assertTrue(list.size() == 0);
        } catch (ValidationException e) {
            Assert.fail("Erreur lors de l'éxécution de la méthode");
        }
    }

    @Test
    public void getClientProductsAvailable() {
        String newClientName = "Client 1";
        int statusToAdd = 1;
        try {
            bankService.AddClientByName(newClientName, 1);

            List<Product> list = bankService.getClientProductsAvailable(newClientName);

            Assert.assertTrue(list != null);

        } catch (ValidationException e) {
            Assert.fail("Erreur lors de l'éxécution de la méthode");
        }
    }


}
