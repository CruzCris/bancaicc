Projet Technique 

Cette version implement l'API avec certains fonctionnalités de la Banque.

Dans le répertoire du projet exécuter la commande

            mvn clean install
            
            
A partir du repertoire target exécuter la commande 

    java -jar bankAPI-0.0.1-SNAPSHOT.jar

Dans une navigateur explorer la page :

    http://localhost:8080/iccBank/swagger-ui.html

Pour tester avec Swagger


---------------

Fichier Shell pour exécuter les commandes de l'application des Employés

    employee.sh

*Commandes Disponibles*

    --add

    --avail


Fichier Shell pour exécuter les commandes de l'application des Clients

    client.sh
    
**Commandes Disponibles**

    --avail