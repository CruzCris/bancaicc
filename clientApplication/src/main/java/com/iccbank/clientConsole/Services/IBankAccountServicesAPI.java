package com.iccbank.clientConsole.Services;

import com.iccbank.bankAPI.contract.api.client.model.Product;

import java.util.List;

public interface IBankAccountServicesAPI {

    List<Product> GetAvailableAccesProducts(String name) throws Exception;
}
