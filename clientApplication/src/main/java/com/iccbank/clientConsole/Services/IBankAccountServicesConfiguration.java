package com.iccbank.clientConsole.Services;

import com.iccbank.bankAPI.contract.api.client.AccountControllerApi;
import com.iccbank.bankAPI.contract.api.client.ClientControllerApi;
import com.iccbank.bankAPI.contract.api.client.handler.ApiClient;

interface IBankAccountServicesConfiguration {

    AccountControllerApi bankAPI();

    ApiClient apiClient();
}
