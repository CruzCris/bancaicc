package com.iccbank.clientConsole.Services;

import ch.qos.logback.core.encoder.EchoEncoder;
import com.iccbank.bankAPI.contract.api.client.AccountControllerApi;
import com.iccbank.bankAPI.contract.api.client.ClientControllerApi;
import com.iccbank.bankAPI.contract.api.client.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankAccountServicesAPI implements IBankAccountServicesAPI{

    @Autowired
    private AccountControllerApi accountControllerApi;

    public BankAccountServicesAPI(AccountControllerApi accountControllerApi)    {
        this.accountControllerApi =accountControllerApi;
    }


    @Override
    public List<Product> GetAvailableAccesProducts(String name) throws Exception {
        try {
            return accountControllerApi.getClientAvailableProducts(name);
        }
        catch (Exception e) {
            throw new Exception("Erreur provenant du Systeme de la Banque. Détails :" + e.getMessage());
        }
    }
}
