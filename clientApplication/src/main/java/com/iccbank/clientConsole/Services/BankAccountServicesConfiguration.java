package com.iccbank.clientConsole.Services;

import com.iccbank.bankAPI.contract.api.client.AccountControllerApi;
import com.iccbank.bankAPI.contract.api.client.ClientControllerApi;
import com.iccbank.bankAPI.contract.api.client.handler.ApiClient;
import com.iccbank.bankAPI.contract.api.client.model.Product;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class BankAccountServicesConfiguration implements IBankAccountServicesConfiguration {


    @Bean
    @Override
    public AccountControllerApi bankAPI() {
        return new AccountControllerApi();
    }

    @Bean
    @Override
    public ApiClient apiClient() {
        return new ApiClient();
    }
}
