package com.iccbank.clientConsole;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientConsoleApplication {

    private static Logger LOG = LoggerFactory.getLogger(ClientConsoleApplication.class);

    public static void main(String[] args) {
        LOG.info("*** IccBank- Application de Services de la Banque- Profil Client");
        System.exit(SpringApplication.exit(SpringApplication.run(ClientConsoleApplication.class, args)));
        LOG.info("APPLICATION FINISHED");
    }
}
