package com.iccbank.clientConsole.Logic;

import com.iccbank.bankAPI.contract.api.client.model.Product;

import java.util.List;

public interface IManageAccount {
    List<Product> GetAvailableAccesProducts(String name) throws Exception;
}
