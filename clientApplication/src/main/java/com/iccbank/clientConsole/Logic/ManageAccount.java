package com.iccbank.clientConsole.Logic;

import com.iccbank.bankAPI.contract.api.client.model.Product;
import com.iccbank.clientConsole.Services.BankAccountServicesAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ManageAccount implements IManageAccount {

    @Autowired
    private BankAccountServicesAPI bankAccountServicesAPI;

    public ManageAccount(BankAccountServicesAPI  bankAccountServicesAPI) {
        this.bankAccountServicesAPI =bankAccountServicesAPI;
    }


    @Override
    public List<Product> GetAvailableAccesProducts(String name) throws Exception {
        try {
            return bankAccountServicesAPI.GetAvailableAccesProducts(name);
        }
        catch (Exception e) {
            throw e;
        }
    }
}
