package com.iccbank.clientConsole.UICommandLine;


import com.iccbank.clientConsole.UICommandLine.CommandLineClientParser;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.stereotype.Component;
import picocli.CommandLine;
import picocli.CommandLine.IFactory;

@Component
public class ClientAppRunner implements CommandLineRunner, ExitCodeGenerator {

    private final CommandLineClientParser cmdLineClientParse;

    private final IFactory factory;

    private int exitCode;

    public ClientAppRunner(CommandLineClientParser cmdLineClientParse, IFactory factory) {
        this.cmdLineClientParse = cmdLineClientParse;
        this.factory = factory;
    }

    @Override
    
    public void run(String... args) throws Exception {
        CommandLine cmdLine = null;
        try {
            cmdLine =new CommandLine(cmdLineClientParse, factory);
            exitCode=  cmdLine.execute(args);
            if (exitCode == -200) {
                cmdLine.usage(System.err);
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public int getExitCode() {
        return exitCode;
    }
}
