package com.iccbank.clientConsole.UICommandLine;

import com.iccbank.bankAPI.contract.api.client.model.Client;
import com.iccbank.bankAPI.contract.api.client.model.Product;
import com.iccbank.clientConsole.Logic.ManageAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import javax.validation.constraints.Null;
import java.util.List;
import java.util.concurrent.Callable;

@Component
@CommandLine.Command(name="client")
public class CommandLineClientParser implements Callable<Integer> {

    @Autowired
    private ManageAccount manageAccount;

    public CommandLineClientParser(ManageAccount manageAccount) {
        this.manageAccount = manageAccount;
    }

    @CommandLine.Option( names = {"-n" } )
    private String clientName;

    @CommandLine.ArgGroup(exclusive = true)
    SubCommandsMutuallyExclusive exclusiveCmd;

    static class SubCommandsMutuallyExclusive {
        @CommandLine.Option( names = {"--status"})
        private boolean status;

        @CommandLine.Option( names = {"--avail"} )
        private boolean available;

        @CommandLine.Option( names = {"--subscribe"} )
        private boolean subscribe;

        @CommandLine.Option( names = {"--unsubscribe"} )
        private boolean unsubscribe;
    }

    @Override
    public Integer call() throws Exception {
        Client client;

        if (exclusiveCmd == null) {
           System.out.println("Aucune option selectionnee");
           return -200;
        }

        if (exclusiveCmd.available && clientName != null)
            try {
                System.out.println("Available commandExecute");
                List<Product> products = manageAccount.GetAvailableAccesProducts(clientName);
                printProducts(clientName, products);
                return 23;
            }
            catch (Exception e) {
                System.out.println("Une erreur est survenu lors de l'éxecution de la commande:" + e.getMessage());
                return -10;
            }

        System.out.println("Aucune Commande à exécuter");
        return -1;
    }

    private void printProducts(String clientName, List<Product> products){
        if (!(products == null) && !products.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            final StringBuilder append = sb.append(CommandLine.Help.Ansi.AUTO.string("@|bold,green,underline PRODUITS DISPONIBLES POUR SOUSCRIPTION POUR LE CLIENT <<" + clientName + ">>\n|@"));
            for (Product p : products) {
                sb.append(CommandLine.Help.Ansi.AUTO.string("@|bold - " + p.getNom() + "\n|@"));
            }

            System.out.println(sb.toString());
        }
    }
}
