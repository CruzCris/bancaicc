package Logic;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.iccbank.bankAPI.contract.api.client.ClientControllerApi;
import com.iccbank.bankAPI.contract.api.client.model.Client;

import com.iccbank.employeeConsole.Services.BankEmployeeAPI;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.mockito.InjectMocks;
import org.springframework.http.HttpStatus;

import javax.xml.bind.ValidationException;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.mockito.Mockito.when;

/** 
* ManageClients Tester. 
* 
* @author <Authors name> 
* @since <pre>Apr 26, 2020</pre> 
* @version 1.0 
*/ 
public class ManageClientsTest {

    @InjectMocks
    public BankEmployeeAPI bankEmployeeAPI;

    @InjectMocks
    public ClientControllerApi clientControllerApi;

    public WireMockServer wireMockServer = new WireMockServer();

@Before
public void before() throws Exception {
    clientControllerApi = new ClientControllerApi();
    bankEmployeeAPI =new BankEmployeeAPI(clientControllerApi);

}

@After
public void after() throws Exception {

}

/** 
* 
* Method: AddClient(String name) 
* 
*/ 
@Test
public void testAddClient() throws Exception { 
//TODO: Test goes here...

    String name = "ClientTest";
    Client  clientToReturn = new Client();
    clientToReturn.setName(name);
    clientToReturn.setStatus(Client.StatusEnum.BASIC);

    setStubForAdd(name);

    wireMockServer.start();

   // when(clientControllerApi.addNewClient(name)).thenReturn(clientToReturn);

    Client clientReturned = bankEmployeeAPI.AddClient(name);

    Assert.assertTrue(clientReturned.getName().compareTo(name)== 0);
    Assert.assertSame(clientReturned.getStatus(), Client.StatusEnum.BASIC);
    wireMockServer.stop();
} 

/** 
* 
* Method: GetAllClients() 
* 
*/ 
@Test
public void testGetAllClients() throws Exception {
    String newClientName = "Client 1";
    int statusToAdd = 1;
    try {
        setStubForAdd(newClientName);
        setStubForGetAllClients(newClientName);
        wireMockServer.start();
        Client clientReturned = bankEmployeeAPI.AddClient(newClientName);

        List<Client> listResult = bankEmployeeAPI.GetAllClients();

        Assert.assertTrue(listResult.size() == 1);
        Assert.assertTrue(listResult.get(0).getName().compareTo(newClientName)== 0);
        Assert.assertTrue(listResult.get(0).getStatus() == Client.StatusEnum.BASIC);

    } catch (ValidationException e) {
        Assert.fail("Erreur lors de l'éxécution de la méthode");
    }
    finally {
        wireMockServer.stop();
    }

}

    private void setStubForAdd(String name) {
        wireMockServer.stubFor(post(urlEqualTo("/iccBank/Clients/AddClient"))
            .withRequestBody(containing(name))
            .willReturn(aResponse().withStatus(HttpStatus.OK.value())
            .withHeader("Content-Type", "application/json")
             .withBody( "{\"productSubscriptions\": [],\"name\": \"" + name +"\",\"status\": \"BASIC\"}"))
       );


    //.withBody( "{\"" + name + "}")));
}

    private void setStubForGetAllClients(String name) {
        wireMockServer.stubFor(get(urlEqualTo("/iccBank/Clients/getAllClients"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", "application/json")
                .withBody( "[{\"productSubscriptions\": [],\"name\": \"" + name + "\",\"status\": \"BASIC\"}]"))
        );
    }
} 
