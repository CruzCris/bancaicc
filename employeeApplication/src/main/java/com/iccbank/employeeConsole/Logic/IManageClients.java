package com.iccbank.employeeConsole.Logic;

import com.iccbank.bankAPI.contract.api.client.model.Client;

import java.util.List;

public interface IManageClients {

    Client AddClient(String name) throws Exception;

    List<Client> GetAllClients();
}
