package com.iccbank.employeeConsole.Logic;

import com.iccbank.bankAPI.contract.api.client.model.Client;
import com.iccbank.employeeConsole.Services.BankEmployeeAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ManageClients implements IManageClients {

    @Autowired
    private BankEmployeeAPI bankEmployeeAPI;

    public ManageClients(BankEmployeeAPI  bankEmployeeAPI) {
        this.bankEmployeeAPI =bankEmployeeAPI;
    }

    @Override
    public Client AddClient(String name) throws Exception {
        try {
            return bankEmployeeAPI.AddClient(name);
        }
        catch (Exception e) {
            throw e;
        }
     }

    @Override
    public List<Client> GetAllClients() {
        return bankEmployeeAPI.GetAllClients();
    }
}
