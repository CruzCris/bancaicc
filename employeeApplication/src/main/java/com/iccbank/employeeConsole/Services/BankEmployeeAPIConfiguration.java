package com.iccbank.employeeConsole.Services;

import com.iccbank.bankAPI.contract.api.client.ClientControllerApi;
import com.iccbank.bankAPI.contract.api.client.handler.ApiClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BankEmployeeAPIConfiguration implements IBankEmployeeAPIConfiguration {

    @Bean
    public ClientControllerApi bankAPI(){
        return new ClientControllerApi();
    }

    @Bean
    public ApiClient apiClient() {
        return new ApiClient();
    }
}
