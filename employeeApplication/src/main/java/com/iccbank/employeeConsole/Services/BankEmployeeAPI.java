package com.iccbank.employeeConsole.Services;

import com.iccbank.bankAPI.contract.api.client.ClientControllerApi;
import com.iccbank.bankAPI.contract.api.client.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@Service
public class BankEmployeeAPI implements IBankEmployeeAPI {

    @Autowired
    private ClientControllerApi clientControllerApi;

    public BankEmployeeAPI(ClientControllerApi bankControllerApi)    {
        this.clientControllerApi =bankControllerApi;
    }

    @Override
    public Client AddClient(String name) throws Exception {
        Client client;
        try {
            return clientControllerApi.addNewClient(name);
        }
        catch (HttpClientErrorException e) {
            throw new Exception("Erreur de communication avec la Banque ou lors de la sauvegarde :" + e.getMessage());
        }
        catch (Exception e) {
            throw new Exception("Erreur general d'ajout du client :" + e.getMessage());
        }
    }

    @Override
    public List<Client> GetAllClients() {
        try {
            return clientControllerApi.getAllClients();
        }
        catch (HttpClientErrorException e) {
        }
        catch (Exception e) {

        }
        return null;
    }
}
