package com.iccbank.employeeConsole.Services;

import com.iccbank.bankAPI.contract.api.client.ClientControllerApi;
import com.iccbank.bankAPI.contract.api.client.handler.ApiClient;
import org.springframework.context.annotation.Bean;

public interface IBankEmployeeAPIConfiguration {

    ClientControllerApi bankAPI();

    ApiClient apiClient();
}