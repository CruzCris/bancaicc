package com.iccbank.employeeConsole.Services;

import com.iccbank.bankAPI.contract.api.client.model.Client;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public interface IBankEmployeeAPI {

    Client AddClient(String name) throws Exception;

    List<Client> GetAllClients();

}
