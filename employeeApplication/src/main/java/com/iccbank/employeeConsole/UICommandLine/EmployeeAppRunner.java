package com.iccbank.employeeConsole.UICommandLine;


import com.iccbank.employeeConsole.UICommandLine.CommandLineEmployeeParser;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.stereotype.Component;
import picocli.CommandLine;
import picocli.CommandLine.IFactory;

@Component
public class EmployeeAppRunner implements CommandLineRunner, ExitCodeGenerator {

    private final CommandLineEmployeeParser cmdLineEmployeeParser;

    private final IFactory factory;

    private int exitCode;

    public EmployeeAppRunner(CommandLineEmployeeParser cmdLineEmployeeParser, IFactory factory) {
        this.cmdLineEmployeeParser = cmdLineEmployeeParser;
        this.factory = factory;
    }

    @Override
    public void run(String... args) throws Exception {
        exitCode = new CommandLine(cmdLineEmployeeParser,factory).execute(args);
    }

    @Override
    public int getExitCode() {
        return exitCode;
    }
}
