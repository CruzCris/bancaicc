package com.iccbank.employeeConsole.UICommandLine;

import com.iccbank.bankAPI.contract.api.client.model.Client;
import com.iccbank.employeeConsole.Logic.IManageClients;
import com.iccbank.employeeConsole.Logic.ManageClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.util.concurrent.Callable;

import static picocli.CommandLine.*;

@Component
@Command (name="employee", mixinStandardHelpOptions = true)
public class CommandLineEmployeeParser implements Callable<Integer> {

    @Autowired
    private ManageClients manageClients;

    public CommandLineEmployeeParser(ManageClients manageClients) {
        this.manageClients = manageClients;
    }

    @Option( names = {"--add"} )
    private String clientNameToAdd;

    @CommandLine.Option( names = {"--avail"} )
    private String clientNameAvail;


    @Override
    public Integer call() throws Exception {
        Client client;
        if (clientNameToAdd != null && !clientNameToAdd.isEmpty()) {
            try {
                System.out.println("ADD commandExecute");
                client= manageClients.AddClient(clientNameToAdd);
                System.out.println("Le client " + client.getName()  + "a été ajouté au Système avec le statut iniital" + client.getStatus());
                return 23;
            }
            catch (Exception e) {
                System.out.println("Une erreur est survenu lors de l'éxecution de la commande:" + e.getMessage());
                return -1;
            }
        }

        if (clientNameAvail != null && !clientNameAvail.isEmpty()) {
            System.out.println("AVAILABLE commandExecute");
            return 15;
        }

        System.out.println("Aucune Commande à exécuter");
        return -1;
    }
}