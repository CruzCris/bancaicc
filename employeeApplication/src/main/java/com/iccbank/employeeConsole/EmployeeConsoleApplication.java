package com.iccbank.employeeConsole;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeConsoleApplication  {

    private static Logger LOG = LoggerFactory.getLogger(EmployeeConsoleApplication.class);


    public static void main(String[] args) {
        LOG.info("*** IccBank- Application Gestion de la Banque - Profil Employé");
        System.exit(SpringApplication.exit(SpringApplication.run(EmployeeConsoleApplication.class, args)));
        LOG.info("APPLICATION FINISHED");
    }
}